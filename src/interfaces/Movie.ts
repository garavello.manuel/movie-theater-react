export interface Movies {
    id: Number,
    title: string,
    overview: string,
    vote_average: Number,
    backdrop_path: string,
    poster_path: string
}

export interface Action {
    type: string,
    payload: Movies
}