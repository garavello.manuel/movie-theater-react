import './App.scss';
import Navbar from './components/layouts/Navbar';
import { useEffect, useState } from 'react';
import { Movies } from './interfaces/Movie';
import Banner from './components/Banner';
import ListMovies from './components/ListMovies';
import { useSelector } from "react-redux";
import { State } from "./state/reducers";

function App() {
  const [moviesList, setMoviesList] = useState<Movies[]>([]);
  const [bannerMovies, setBannerMovies] = useState<Movies[]>([])
  const movieListState: any[] = useSelector((state: State) => state.movieList)

  const getMoviesList = async () => {
    const res = await fetch(`${process.env.REACT_APP_BASE_URL}discover/movie?api_key=${process.env.REACT_APP_API_KEY}`)
    const data = await res.json();
    if (movieListState.length === 0){
      setMoviesList(data.results);
    } else {
      setMoviesList(movieListState);
    }
  }

  const getBannerMovies = async () => {
    const res = await fetch(`${process.env.REACT_APP_BASE_URL}discover/movie?api_key=${process.env.REACT_APP_API_KEY}`)
    const data = await res.json();
    let array = [];
    for (let i in data.results){
      const bannerImage = await fetch(`https://api.themoviedb.org/3/movie/${data.results[i].id}/images?api_key=3817239857db25016905d54322dc2ad0`)
      const bannerImageResponse = await bannerImage.json();
      array.push({id: data.results[i].id, title: data.results[i].title, overview: data.results[i].overview, vote_average: data.results[i].vote_average, backdrop_path: data.results[i].backdrop_path, poster_path: bannerImageResponse.backdrops[0].file_path})
    }
    setBannerMovies(array);
  }

  useEffect(() => {
    document.title = "Local Movie Theater"
    getBannerMovies()
  },[])

  useEffect(() => {
    getMoviesList()
  }, [movieListState])
  
  return (
    <div className="App">
      <Navbar />
      <Banner moviesListBanner={bannerMovies} />
      <ListMovies moviesList={moviesList} />
    </div>
  );
}

export default App;
