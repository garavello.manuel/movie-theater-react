import { ChangeEvent, useState } from 'react';
import { FaSearch } from 'react-icons/fa'
import { BiDotsVerticalRounded } from 'react-icons/bi'
import { useDispatch } from 'react-redux';
import { queryMovie, mostRated } from '../../state/actions';
import { AiFillStar } from 'react-icons/ai';
import { Movies } from '../../interfaces/Movie';
import { IconContext } from 'react-icons/lib';


export default function Navbar() {
    const [SearchState, setSearchState] = useState<Boolean>(false);
    const [menuState, setMenuState] = useState<Boolean>(false);

    const ratedBox = document.querySelector(".rated-search__div")

    const showSearchInput = () => {
        setSearchState(!SearchState);
        if(ratedBox?.classList.contains("active") && SearchState === true) ratedBox?.classList.remove("active");
        const search_input = document.querySelector<HTMLInputElement>('.search-box__input');
        SearchState ? search_input?.classList.add("active") : search_input?.classList.remove("active");
        if (SearchState) return search_input?.focus();
    }
    const showMenu = () => {
        setMenuState(!menuState);
        const btn_burger = document.querySelector<HTMLElement>('.btn-burger');
        menuState ? btn_burger?.classList.add("active") : btn_burger?.classList.remove("active");
    }
    const showRatedSearch = () => {
        if(ratedBox?.classList.contains("active") && SearchState === true){
            ratedBox?.classList.remove("active");
            showSearchInput()
        } else {
            ratedBox?.classList.add("active");
            showSearchInput()
        }
    }
    const dispatch = useDispatch()

    const handleQueryMovie = async (e: ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value
        if(value !== ""){
            const testing = await fetch(`${process.env.REACT_APP_BASE_URL}search/movie?api_key=${process.env.REACT_APP_API_KEY}&language=en-US&query=${value}&page=1&include_adult=false`)
            const res = await testing.json()
            dispatch(queryMovie(res.results))
        } else {
            dispatch(queryMovie([]))
        }
    }

    const handleRated = async ({target}: ChangeEvent<HTMLInputElement>, i: number) => {
        const res = await fetch(`${process.env.REACT_APP_BASE_URL}discover/movie?api_key=${process.env.REACT_APP_API_KEY}&page=180`)
        const data = await res.json();
        let result = null;

        switch (target.id) {
            case "star_5":
                result = data.results.filter((res:Movies) => (res.vote_average >= 0 && res.vote_average <= 2))
                return dispatch(mostRated(result));
            case "star_4":
                result = data.results.filter((res:Movies) => (res.vote_average >= 2 && res.vote_average <= 4))
                return dispatch(mostRated(result));
            case "star_3":
                result = data.results.filter((res:Movies) => (res.vote_average >= 4 && res.vote_average <= 6))
                return dispatch(mostRated(result));
            case "star_2":
                result = data.results.filter((res:Movies) => (res.vote_average >= 6 && res.vote_average <= 8))
                return dispatch(mostRated(result));
            case "star_1":
                result = data.results.filter((res:Movies) => (res.vote_average >= 8 && res.vote_average <= 10))
                return dispatch(mostRated(result));
            default:
                return dispatch(mostRated([]));
        }
    }

    return (
        <section className="navbar-container">
            <figure className="logo">
                <img src={'./logo512.png'} alt="logo" />
            </figure>
            <div className="search-box">
                <div className="search-box__icon" onClick={showSearchInput}>
                    <FaSearch />
                </div>
                <input type="search" name="search" className="search-box__input active" placeholder="Search your movie" onChange={handleQueryMovie} />
                <div className="search-box__dotted-icon" onClick={showRatedSearch}>
                    <IconContext.Provider value={{ color: "white", size: "1.5em" }}>
                        <BiDotsVerticalRounded />
                    </IconContext.Provider>
                </div>
                <div className="rated-search__div">
                    <p className="rated-search__p">Rating:</p>
                    {/* {rateStar.map((rate, i) => ( */}
                        <div className="rated-search-icon-star__div">
                            <input type="radio" className="radio-input" name="star" id="star_1" onChange={(e) => handleRated(e, 0)}/>
                            <label htmlFor="star_1">
                                <AiFillStar />
                            </label>
                            <input type="radio" className="radio-input" name="star" id="star_2" onChange={(e) => handleRated(e, 1)}/>
                            <label htmlFor="star_2">
                                <AiFillStar />
                            </label>
                            <input type="radio" className="radio-input" name="star" id="star_3" onChange={(e) => handleRated(e, 2)}/>
                            <label htmlFor="star_3">
                                <AiFillStar />
                            </label>
                            <input type="radio" className="radio-input" name="star" id="star_4" onChange={(e) => handleRated(e, 3)}/>
                            <label htmlFor="star_4">
                                <AiFillStar />
                            </label>
                            <input type="radio" className="radio-input" name="star" id="star_5" onChange={(e) => handleRated(e, 4)}/>
                            <label htmlFor="star_5">
                                <AiFillStar />
                            </label>
                        </div>
                    {/* ))} */}
                </div>
            </div>
            <nav className="navbar__">
                <div className="btn-burger" onClick={showMenu}>
                    <div className="btn-burger-line"></div>
                </div>
                <ul className="navbar__ul">
                    <li className="navbar__li">Home</li>
                    <li className="navbar__li">Popularies</li>
                    <li className="navbar__li">Exclusives</li>
                </ul>
            </nav>
        </section>
    )
}
