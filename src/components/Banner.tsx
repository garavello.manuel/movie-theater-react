import { useEffect } from 'react';
import { AiOutlinePlus } from 'react-icons/ai';
import { Movies } from '../interfaces/Movie';

interface Props {
    moviesListBanner: Movies[]
}

export default function Banner({moviesListBanner}: Props) {
    const readMore = (e: any, i: number) => {
        const text_elements = document.querySelectorAll('.description-movie__p')
        if (text_elements.item(i).classList.contains("hidden")) {
            text_elements.item(i).classList.remove("hidden")
            e.target.innerText = "Read less"
        } else {
            text_elements.item(i).classList.add("hidden")
            e.target.innerText = "Read more"
        }
    }
    useEffect(() => {
        const text_elements = document.querySelectorAll('.description-movie__p')
        for (let i in text_elements){
            const height = text_elements.item(Number(i))?.clientHeight;
            if(height >= 100) {
                document.querySelectorAll('.description-movie__p').item(Number(i))?.classList.add('hidden')
            }
        }
    })
    return (
        <section className="banner-container">
            {moviesListBanner.slice(0, 5).map((movieBanner, i) => (
                <div className="banner-box" key={i}>
                    <div className="description-movie">
                        <h1 className="description-movie__h1">{movieBanner.title}</h1>
                        <p className="description-movie__p">{movieBanner.overview}</p>
                        {
                            document.querySelectorAll('.description-movie__p').item(i)?.clientHeight > 147 && 
                            <button onClick={(e) => readMore(e, i)} className="read-more__button">Read more</button>
                        }
                        <div className="btn-box">
                            <button type="button" className="btn-primary">Trailer Movie</button>
                            <button type="button" className="btn-secondary-rounded">
                                <AiOutlinePlus />
                            </button>
                        </div>
                    </div>
                    <figure className="banner-movie">
                        <img src={process.env.REACT_APP_ORIGINAL_IMAGES_URL + movieBanner.poster_path} alt={movieBanner.title} className="banner-movie__img" />
                    </figure>
                </div>
            ))}
        </section>
    )
}
