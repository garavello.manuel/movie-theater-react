import { Movies } from "../interfaces/Movie";
import { AiFillPlayCircle, AiFillPlusCircle, AiFillStar } from "react-icons/ai";
import { IconContext } from "react-icons";

interface Props {
    moviesList: Movies[];
}
export default function ListMovies({moviesList}: Props) {
    return (
        <div className="list-movies-container">
                {moviesList.map((movie, i) => (
                    <section className="list-movies-box" key={i}>
                        <img src={movie.backdrop_path === null ? '/logo512.png' : process.env.REACT_APP_IMAGES_URL + movie.backdrop_path} alt="movie" className="list-movies-box__img" />
                        <div className="list-movies-box__title">
                            <h1 className="list-movies-box__h1">{ movie.title}</h1>
                            <div className="rated-box">
                                <p className="rated-box__p">{movie.vote_average}</p>
                                <AiFillStar />
                            </div>
                        </div>
                        <div className="btn-box-list">
                            <IconContext.Provider value={{ color: "white", size: "2.5em" }}>
                                <button type="button">
                                    <AiFillPlayCircle />
                                </button>
                                <button type="button">
                                    <AiFillPlusCircle />
                                </button>
                            </IconContext.Provider>
                        </div>
                    </section>
                    )
                )}
            </div>
    )
}
