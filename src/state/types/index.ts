export enum ActionsTypes {
    MOST_RATED = 'most_rated',
    QUERY_MOVIE = 'query_movie'
}