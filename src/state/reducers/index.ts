import { combineReducers } from 'redux';
import { movieListReducer } from './MovieListReducer';

export const reducers = combineReducers({
    movieList: movieListReducer
})

export type State = ReturnType<typeof reducers>