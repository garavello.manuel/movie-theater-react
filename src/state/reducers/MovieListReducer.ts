import { Action } from "../../interfaces/Movie";
import { ActionsTypes } from "../types";

let initialState: any = []

export const movieListReducer = (state: any = initialState, action: Action) => {
    switch (action.type) {
        case ActionsTypes.MOST_RATED:
            return state = action.payload;
        case ActionsTypes.QUERY_MOVIE:
            return state = action.payload;
        default:
            return state
    }
}