import { Movies } from "../../interfaces/Movie"
import { ActionsTypes } from "../types"

export const mostRated = (movieList: Movies | any[]) => {
    return {
            type: ActionsTypes.MOST_RATED,
            payload: movieList
        }
}

export const queryMovie = (movieList: Movies | any[]) => {
    return {
        type: ActionsTypes.QUERY_MOVIE,
        payload: movieList
    }
}